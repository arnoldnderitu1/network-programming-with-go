# Network programming using Go - Introduction

## Distributed Systems
Think of a group of computers working together as to appear as a single computer to the end-user.

Some characteristics of distributed systems:
1. Have a shared state
2. Operate concurrently 
3. Fail independently 

![](1_n-3Db0bssxh7B-A0Jndx0A.png)

A database running on multiple machines at the same time is an example of a distributed system. Whenever a user inserts information to the DB all nodes (computers) will have a similar state.

## Why distribute a system
A distributed system makes it possible to **scale horizontally**. 

In the case of a database running on a single machine, the only way to handle more traffic would be to upgrade the hardware the DB is running on (e.g increase processing power; or storage) - **vertical scaling** .

After a certain point even the best hardware is not sufficient for enough traffic. 

**Scaling horizontally** - requires adding more computers rather than upgrading the hardware of a single one. 

![](1_ulnsAJdpvet9EW4bKMK-Rw.png)

Vertical scalability costs rise sharply after a certain point, whereas horizontal scaling becomes **much cheaper** after a certain threshold.

Other benefits of distributing systems:
* **Fault tolerance** - a cluster of 10 machines across two data centers is inherently more fault tolerant than a single machine. Even if one of the data centers catches fire, your application will still be working.

* **Low Latency** - The time for a network packet to travel the world is physically bounded by the speed of light. E.g the shortest possible time for a request's **round-trip time** (that is, go back and forth) in a fiber-optic cable from one city to another may be 160ms. Distributed systems allow you to have a node in both cities, allowing traffic to hit the node that is closest to it.